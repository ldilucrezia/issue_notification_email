# Redmine per-issue notification email plugin
require 'redmine'

#RAILS_DEFAULT_LOGGER.info 'Starting Customer plugin for RedMine'

Redmine::Plugin.register :issue_notification_email do
  name 'Per-issue notification email'
  author 'Luciano Di Lucrezia'
  description 'This plugin sends an e-mail to an address specified in the issue in addition to the project-wide notification address.'
  version '0.0.1'

  url 'https://www.shi.it' if respond_to? :url
  author_url 'http://www.shi.it' if respond_to? :author_url

  Issue # force class load
  require(File.join(File.dirname(__FILE__), '../app/models/issue.rb'))
end
