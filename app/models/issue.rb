class Issue < ActiveRecord::Base

  def self.notification_email_custom_field
    @notification_email_custom_field ||= CustomField.find_by_type_and_name('IssueCustomField', 'Notify address')
  end

  def recipients_with_issue_specific_notification_emails
    base_recipients = recipients_without_issue_specific_notification_emails
    custom_field    = self.class.notification_email_custom_field
    return base_recipients unless custom_field.is_a?(IssueCustomField)
    [ base_recipients, custom_field_value(custom_field) ].flatten.compact.uniq
  end

  alias_method_chain :recipients, :issue_specific_notification_emails

end
